<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends RestController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $response = function () {
            return User::all(['id', 'name'])->sortBy('name');
        };

        return $this->makeResponse($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $response = function (Request $request) {
            $user = new User();
            $user->name =  $request->json('name');
            $user->save();
            return [
                'user_id' => $user->id,
                'user_name' => User::getUserName($user->id),
            ];
        };

        return $this->makeResponse($response, $request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $response = function (Request $request) {
            return User::where([
                'id' => $request->route('user')
            ])->firstOrFail(['id', 'name']);
        };

        return $this->makeResponse($response, $request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $response = function(Request $request) {
            $userName = $request->json('name');
            $userID = $request->route('user');
            $oldUserName = User::getUserName($userID);

            User::where([
                'id' => $userID
            ])->update([
                'name' => $userName
            ]);

            $newUserName = User::getUserName($userID);
            return [
                'user_id' => $userID,
                'user_name' => $newUserName,
                'success' => "User was successfully updated, the name was changed from $oldUserName to $newUserName",
            ];
        };

        return $this->makeResponse($response, $request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $response = function(Request $request) {
            $userID = $request->route('user');
            $userName = User::getUserName($userID);

            User::destroy($userID);

            return [
                'user_id' => $userID,
                'user_name' => $userName,
                'success' => "User $userName with id \"$userID\" was successfully deleted",
            ];
        };

        return $this->makeResponse($response, $request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function messagesByUser(Request $request)
    {
        $response = function (Request $request) {
            $userID = $request->route('user_id');
            $user = User::where('id', $userID)->firstOrFail();
            return $user->messages->all(['id', 'date', 'message'])->sortByDesc('date');
        };

        return $this->makeResponse($response, $request);
    }
}