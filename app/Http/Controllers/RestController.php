<?php

namespace App\Http\Controllers;


class RestController extends Controller
{
    /**
     * @return array _token
     */
    protected function tokenHeader()
    {
        return ['_token' => csrf_token()];
    }

    /**
     * @param callable $callback
     * @param mixed $param
     * @return \Illuminate\Http\JsonResponse
     */
    protected function makeResponse($callback, $param = false)
    {
        try {
            if (!$param) {
                $response = $callback();
            } else {
                $response = $callback($param);
            }
            $status = 200;
        } catch (\Exception $e) {
            $status = 400;
            $response = [
                'error' => $e->getMessage(),
            ];
        } finally {
            return response()->json($response, $status, $this->tokenHeader());
        }
    }
}