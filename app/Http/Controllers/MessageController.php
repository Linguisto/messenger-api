<?php

namespace App\Http\Controllers;

use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MessageController extends RestController
{

    public function index() {
        try {
            $response = Message::all(['id', 'user_id', 'date', 'message'])->sortByDesc('date');
            $status = 200;
        } catch (\Exception $e) {
            $status = 400;
            $response = [
                'error' => $e->getMessage()
            ];
        } finally {
            return response()->json($response, $status, $this->tokenHeader());
        }
    }

    public function store(Request $request) {
        try {
            $message = new Message();
            $message->user_id = $request->json('user');
            $message->message = $request->json('message');
            $message->date = Carbon::now();
            $message->save();

            $status = 200;
            $response = [
                'success' => 'Message successfully sent'
            ];

        } catch (\Exception $e) {
            $status = 400;
            $response = [
                'error' => $e->getMessage()
            ];
        } finally {
            return response()->json($response, $status, $this->tokenHeader());
        }
    }

    public function show(Request $request) {
        $message_id = $request->route('message');
        try {
            $response = Message::where([
                'id' => $message_id
            ])->firstOrFail(['id', 'user_id', 'date', 'message']);
            $status = 200;
        } catch (\Exception $e) {
            $status = 400;
            $response = [
                'error' => $e->getMessage()
            ];
        } finally {
            return response()->json($response, $status, $this->tokenHeader());
        }
    }

    public function update(Request $request) {
        $message_id = $request->route('message');
        try {
            Message::where([
                'id' => $message_id
            ])->update([
               'message' => $request->json('message'),
                'date' => Carbon::now()
            ]);

            $response = [
                'success' => 'Message successfully changed'
            ];
            $status = 200;
        } catch (\Exception $e) {
            $status = 400;
            $response = [
                'error' => $e->getMessage()
            ];
        } finally {
            return response()->json($response, $status, $this->tokenHeader());
        }
    }

    public function destroy(Request $request) {
        try {
            $message_id = $request->route('message');
            Message::destroy($message_id);
            $response = [
                'success' => 'Message successfully deleted'
            ];
            $status = 200;
        } catch (\Exception $e) {
            $status = 400;
            $response = [
                'error' => $e->getMessage()
            ];
        } finally {
            return response()->json($response, $status, $this->tokenHeader());
        }
    }

    public function userByMessage(Request $request) {
        try {
            $message_id = $request->route('message_id');
            $message = Message::where('id', $message_id)->firstOrFail();
            $response = $message->user->firstOrFail(['id', 'name']);
            $status = 200;
        } catch (\Exception $e) {
            $status = 400;
            $response = [
                'error' => $e->getMessage()
            ];
        } finally {
            return response()->json($response, $status, $this->tokenHeader());
        }
    }
}