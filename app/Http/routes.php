<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('user/{user_id}/messages', 'UserController@messagesByUser');
Route::get('message/{message_id}/user', 'MessageController@userByMessage');

Route::resource('user', 'UserController', ['except' => ['create', 'edit']]);
Route::resource('message', 'MessageController', ['except' => ['create', 'edit']]);