<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $fillable = ['message', 'user_id', 'date', 'created_at', 'updated_at'];

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
