<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = ['name', 'created_at', 'updated_at'];

    public static function getUserName($user_id) {
        return self::where([
            'id' => $user_id
        ])->firstOrFail()->name;
    }

    public function messages() {
        return $this->belongsTo('App\Message', 'id', 'user_id');
    }
}
