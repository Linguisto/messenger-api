Messenger API
=============
Author: Yaroslav Voitenko

Licence: [MIT license](http://opensource.org/licenses/MIT)
___
**Notice: `_token` is in `headers` of every response**
___

##User
- `GET /user` – list all users
- `GET /user/{id}` – show a user by ID
- `POST /user` – create a new user
    - `{"_token":"E8lhbSjxWiQLUgefDQFYduO0xcqeXzQw18mv5c17", "name": "John Doe"}`
- `DELETE /user/{id}` – delete a user by ID
- `PUT/PATCH /user/{id}` – update the information of a user by ID
- `GET /user/{id}/messages` – list all user's messages
___
##Message
- `GET /message` – list all messages
- `GET /message/{id}` – show a message by ID
- `POST /message` – create a new message
    - `{"_token":"E8lhbSjxWiQLUgefDQFYduO0xcqeXzQw18mv5c17", "user":"4", "message":"Sample Text"}`
- `DELETE /message/{id}` – delete a message by ID
- `PUT/PATCH /message/{id}` – update the information of a message by ID
- `GET /message/{id}/user` – get a user by message ID